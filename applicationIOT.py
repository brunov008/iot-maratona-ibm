import wiotp.sdk.device
import random
import time
from watson_developer_cloud import VisualRecognitionV3
import json

""" VISUAL RECOGNITION """
visual_recognition = VisualRecognitionV3(
    version='2018-03-19',
    iam_apikey='O0ijp1URlC8aLQdUTjcdwa0wyl2GrA7Nen61ro6WazFD'
)

""" DEVICE """
myconfig = {
  "identity":{
    "orgId" : "1c3ras",
    "typeId":"freezer-dispositivo",
    "deviceId":"freezer-01"
  },
  "auth":{
    "token": "3nj12kn4jk21n2j3k-nn"
  }
}

def myCommandCallback(cmd):
	print("Command received: %s" % cmd.data)

client = wiotp.sdk.device.DeviceClient(config = myconfig)

client.connect()

while True:
  with open('./teste.jpg', 'rb') as images_file:
  classes = visual_recognition.classify(
    images_file, 
    threshold='0.6', 
    classifier_ids='Maratona-model-desafio5_1587578441').get_result()

	myData = {'temperatura':random.randint(0, 20), status_freezer:classes}

	client.publishEvent(eventId="temperatura",msgFormat="json", data=myData, qos=0, onPublish=None)
	client.commandCallback = myCommandCallback
	time.sleep(3p)
