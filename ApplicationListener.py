import wiotp.sdk.application 
import json

appConfig = {
	"auth":{
		"key":"a-1c3ras-t521q6tfyb",
		"token":"+FvmL7tInphkffIBDf"
	}
}

def myEventCallback(event):
	str = "EVENTFORMAT %s ---- EVENTID'%s' ----- DEVICE [%s] ---- DATA %s ---- TYPEID %s"
	print(str % (event.format, event.eventId, event.device, json.dumps(event.data), event.typeId))
	if event.data['temperatura'] > 30:
		commandData={"cmd":"just do it"}
		client.publishCommand("freezer-dispositivo", "freezer-01", "reboot", "json", commandData)

client = wiotp.sdk.application.ApplicationClient(config=appConfig)
client.connect()
client.subscribeToDeviceEvents()

while True:
	client.deviceEventCallback = myEventCallback